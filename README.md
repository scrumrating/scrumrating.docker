# docker-compose для сайта ScrumRating

Быстро и удобно разварачивает сайт на сервере. *docker-compose* поднимает:
1. RabbitMQ <http://165.22.26.131:15672> (login: guest, password: guest)
2. PostgresSql
3. Nginx
4. Frontend (Blazor) <http://165.22.26.131>
5. Student services - <http://165.22.26.131/api/student/swagger>
6. Team service - <http://165.22.26.131/api/team/swagger>
7. Homework service - <http://165.22.26.131/api/homework/swagger>
8. Lesson service - <http://165.22.26.131/api/lesson/swagger>
9. Rating service - <http://165.22.26.131/api/rating/swagger>
10. Authorization service - <http://165.22.26.131/api/authorization/swagger>
11. Course service - <http://165.22.26.131/api/course/swagger>
12. Summary service - <http://165.22.26.131/api/summary/swagger> *Пока не доступно*

Все сервисы (.net Core WebApi) на 10.05.2020 доступны по адресам выше.

## Структура и описание

Далее описаны папки для чего предназначены. Что хранится в этих папках, то и хранится в контейнер (такой своеобразный проброс).

* DataBase - папка в которой будет хранится база данных. Даже если контейнер упадёт база данных сохранится в этой папке. 
**При запуске на windows машине надо будет удалить из docker-compose следующую строчку**
```
volumes:
      - ${DB_PATH_HOST}:/var/lib/postgresql/data
```
Он не будет сохранять базу в папку из-за конфликта прав доступа linux и windows он требует пользователя sudo которого в windows нет.

* Frontend - в этой папке хранится фронт Blazor.

* Logs - здесь будут логи от Nginx.

* Nginx - конфигурация для веб-сервера, а так же папка с сертификатами(Для использованния https, **надо будет изменить конфигурацию**).

* Service - папка с сервисами, каждый сервис ложим в отдельную папку.

* .env - файл переменных окружения (так проще поддерживать docker-compose).

* docker-compose.yml - не нуждается в представлении.

## Поднятие docker-compose
Для запуска надо открыть каталог с файлом *docker-compose.yml* в терминале(командной строке) и выполнить команду:

```
docker-compose up -d --build
```

## Остановить docker-compose

1. Останваливаем docker-compose командой
```
docker-compose stop
```

2. Ещё чтобы нверняка можно удалить остановленные контейнеры командой
```
docker rm $(docker ps -a -f status=exited -q)
```

## Взаимодействие с отдельным контейнером
После обновления одного из конейнеров не обязательно перезапускать весь *docker-compose*. Перезапускаем контейнер командой:

```
docker restart *название_контейнера*
```

Посмотреть логи контейнера:

```
docker logs *название_контейнера*
```